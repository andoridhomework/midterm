# Author: Anzo Mikutishvili

Marvel Movie list app for btu midterm.

Sign in activity

Sign up activity

Marvel Movies list activity

## Features!

Google Firebase Authentication: email/facebook/gmail/mobile

API data https://www.simplifiedcoding.net/demos/marvel/ 

You can: - Get all popular Marvel movie list from API


### Tech ###

Used Technologies:

[Firebase] - For authentification

[Retrofit] - For rest calls

[MarvelMovie API] - To fetch movies data.

[Kotlin] - To write app