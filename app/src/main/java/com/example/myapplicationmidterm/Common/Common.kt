package com.example.myapplicationmidterm.Common

import com.example.myapplicationmidterm.Interface.RetrofitService
import com.example.myapplicationmidterm.Retrofit.RetrofitClient

object Common {
    private val BASE_URL = "https://www.simplifiedcoding.net/demos/"

    val retrofitService: RetrofitService
        get() = RetrofitClient.getClient(BASE_URL).create(RetrofitService::class.java)
}